﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Reisikulud.Startup))]
namespace Reisikulud
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
