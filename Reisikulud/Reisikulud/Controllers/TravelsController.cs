﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Reisikulud.Models;
using System.IO;

namespace Reisikulud.Controllers
{
    public class TravelsController : Controller
    {
        private BackSeatEntities db = new BackSeatEntities();


        private Dictionary<int, string> Kategooriad = new Dictionary<int, string>
        {
            {0, "Määramata" },
            {1, "Transport" },
            {2, "Majutus" },
            {3, "Toitlustus" },
            {4, "Konverentsid" },
            {5, "Muud" }
        };
        private Dictionary<int, string> Staatused = new Dictionary<int, string>
        {
            {0, "Määramata" },
            {1, "Salvestatud" },
            {2, "Esitatud" },
            {3, "Kinnitatud" },
            {4, "Hüvitatud" },
        };

        // GET: Travels
        //public ActionResult Index()
        //{


        //    var travels = db.Travels.Include(t => t.DataFile).Include(t => t.Person);
        //    return View(travels.ToList());
        //}
        //[Authorize]
        //public ActionResult Index()
        //{

        //    var travels = db.Travels.Where(x => x.Person.Email == User.Identity.Name);
        //    return View(travels);

        //}

        [Authorize]
        public ActionResult Index(string sortKey = "Id", string sortDirection = "Asc", string searchString = "", DateTime? searchstartDate = null, DateTime? searchendDate = null)
        {

            //DateTime limit = DateTime.Now.AddMonths(-6);
            Person person = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            if (person == null) return RedirectToAction("Index", "Home");

            var q = db.Travels.Where(x => true)
                .Where(x => searchString == "" || x.Person.FirstName.Contains(searchString) || x.Person.LastName.Contains(searchString))
                .Where(x => !searchstartDate.HasValue || (searchstartDate >= x.StartDate && searchstartDate <= x.EndDate))
            ;

            if (User.IsInRole("Raamatupidaja")) q = q.Where(x => x.State > 0);
            else if (User.IsInRole("Osakonnajuhataja")) q = q.Where(x => x.Person.DepartmentId == person.Department.Id);
            else q = q.Where(x => x.PersonId == person.Id);



            var qs = q.OrderBy(x => x.Id).Take(20);
            
            ViewBag.SortKey = sortKey;
            ViewBag.SortDirection = sortDirection;
            ViewBag.SearchString = searchString;
            ViewBag.SearchEndDate = searchendDate;
            ViewBag.SearchStartDate = searchstartDate;
            ViewBag.Staatused = Staatused;

            switch (sortKey + sortDirection)
            {
                case "NameAsc": qs = q.OrderBy(x => x.Name); break;
                case "NameDesc": qs = q.OrderByDescending(x => x.Name); break;
                case "StateAsc": qs = q.OrderBy(x => x.State); break;
                case "StateDesc": qs = q.OrderByDescending(x => x.State); break;
                case "StartDateAsc": qs = q.OrderBy(x => x.StartDate); break;
                case "StartDateDesc": qs = q.OrderByDescending(x => x.StartDate); break;
            }

            return View(qs.ToList());
        }
        
        public ActionResult ChangeState(int id, int state)
        {
            Travel travel = db.Travels.Find(id);
            if (travel != null)
            {
                travel.State = state;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        // GET: Travels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            //ViewBag.category = new SelectList(Kategooriad, "Key", "Value");
            //ViewBag.Kategooriad = Kategooriad;

            return View(travel);
        }


        // GET: Travels/Create
        public ActionResult Create()
        {
            Person person = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            if (person == null) return RedirectToAction("Index");


            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", person.Id);
            return View(new Travel { State = 1, PersonId = person.Id, PictureId = null, StartDate = DateTime.Now, EndDate = DateTime.Now.AddDays(7), Category = 0 });
        }

        // POST: Travels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,PersonId,Goal,Category,StartDate,EndDate,State,PictureId")] Travel travel)
        {
            if (ModelState.IsValid)
            {
                if (travel.StartDate == null) travel.StartDate = DateTime.Now;
                db.Travels.Add(travel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", travel.PictureId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", travel.PersonId);
            return View(travel);
        }

        public ActionResult AddExpense(int id, string name, decimal cost, int duration, string description, int category)
        {
            Travel travel = db.Travels.Find(id);
            if (travel != null)
            {
                try
                {
                    travel.Expenses.Add(new Expense
                    {
                        Name = name,
                        Cost = cost,
                        Duration = duration,
                        Description = description,
                        Category = category
                    }
                   );
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewBag.Error = e.Message;
                }
            }

            return RedirectToAction("Edit", new { id });
        }
        public ActionResult RemoveExpense(int id, int expenseId)
        {
            Expense expense = db.Expenses.Find(expenseId);
            if (expense != null)
            {
                db.Expenses.Remove(expense);
                db.SaveChanges();
            }
            return RedirectToAction("Edit", new { id });
        }


        // GET: Travels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", travel.PictureId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", travel.PersonId);
            ViewBag.category = new SelectList(Kategooriad, "Key", "Value");
            ViewBag.Kategooriad = Kategooriad;
            return View(travel);
        }

        // POST: Travels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,PersonId,Goal,Category,StartDate,EndDate,State,PictureId")] Travel travel, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                db.SaveChanges();
                if (file != null && file.ContentLength > 0)  // file on olemas
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);

                        DataFile uusFile = new DataFile
                        {
                            ContentType = file.ContentType,
                            Content = buff,
                            Name = file.FileName.Split('\\').Last()
                        };
                        db.DataFiles.Add(uusFile);
                        db.SaveChanges();
                        travel.PictureId = uusFile.Id;

                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", travel.PictureId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", travel.PersonId);
            return View(travel);
        }



        // GET: Travels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            return View(travel);
        }

        // POST: Travels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Travel travel = db.Travels.Find(id);
            db.Travels.Remove(travel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
