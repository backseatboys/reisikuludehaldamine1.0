﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Reisikulud.Models;

namespace Reisikulud.Models
{
    partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
        public string Nimi => FullName;
    }

    namespace Reisikulud.Controllers
    {

        public class PeopleController : Controller
        {
            private BackSeatEntities db = new BackSeatEntities();
            [Authorize]
            // GET: People
            public ActionResult Index()
            {
                if (Request.IsAuthenticated)

                {
                    AspNetUser user = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                    Person person = db.People.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                    if (person != null)
                    {
                        string fullname = person?.FullName ?? "Tundmatu";
                        ViewBag.FullName = fullname;

                        if (person.Id == person.Department.managerId)
                        {
                            var people = db.People.Include(p => p.DataFile).Include(p => p.Department);
                            return View(people.ToList());
                        }
                        else
                        {
                            var people = /*person.Department.*/db.People.ToList();
                            return View(people.ToList());
                        }

                    }
                }

                return RedirectToAction("Index", "Home");
            }
            // GET: People/Details/5
            public ActionResult Details(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Person person = db.People.Find(id);
                if (person == null)
                {
                    return HttpNotFound();
                }
                return View(person);
            }

            // GET: People/Create
            public ActionResult Create()
            {
                ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name");
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name");
                return View();
            }

            // POST: People/Create
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Email,IK,DepartmentId,PictureId")] Person person)
            {
                //koht, kus saab kontrollida inimeste parameetreid
                if ((person.FirstName ?? "").Length < 2)
                    ModelState.AddModelError("Firstname", "Nimi puudub või liiga lühike");
                if ((person.LastName ?? "").Length < 2)
                    ModelState.AddModelError("Lastname", "Nimi puudub või liiga lühike");
                if ((person.Email ?? "").Length < 8)
                    ModelState.AddModelError("Email", "Meiliaadress puudub või liiga lühike");
                if ((person.IK ?? "").Length < 11)
                    ModelState.AddModelError("IK", "Isikukood puudub või liiga lühike");

                //kontrollid tuleb teha enne seda lauset
                if (ModelState.IsValid)
                {
                    db.People.Add(person);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", person.PictureId);
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
                return View(person);
            }

            // GET: People/Edit/5
            public ActionResult Edit(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Person person = db.People.Find(id);
                if (person == null)
                {
                    return HttpNotFound();
                }
                ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", person.PictureId);
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
                return View(person);
            }

            // POST: People/Edit/5
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Email,IK,DepartmentId,PictureId")] Person person)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(person).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "Name", person.PictureId);
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
                return View(person);
            }

            // GET: People/Delete/5
            public ActionResult Delete(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Person person = db.People.Find(id);
                if (person == null)
                {
                    return HttpNotFound();
                }
                return View(person);
            }

            // POST: People/Delete/5
            [HttpPost, ActionName("Delete")]
            [ValidateAntiForgeryToken]
            public ActionResult DeleteConfirmed(int id)
            {
                Person person = db.People.Find(id);
                db.People.Remove(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                base.Dispose(disposing);
            }
        }
    }
}
