﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reisikulud.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Minu poolest võid end põlema ka panna.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Siin on meie kontaktandmed.";

            return View();

        }
    }
}