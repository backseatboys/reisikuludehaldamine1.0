﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Reisikulud.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Jätta see brauser meelde?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [Display(Name = "Jäta mind meelde?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} peab olema vähemalt {2} märgi pikkune.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita salasõna")]
        [Compare("Password", ErrorMessage = "Salasõnad ei ole samasugused")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Perenimi")]
        public string LastName { get; set; }

        [Required] [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode =true, DataFormatString = "dd.MM.yyyy")]
        [Display(Name = "Sünniaeg")]
        public System.DateTime BirthDate { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = " {0} peab olema vähemalt {2} märgi pikkune.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita salasõna")]
        [Compare("Password", ErrorMessage = "Salasõnad ei ole samasugused.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
